//
//  Landmark.swift
//  landmarkTutorial
//
//  Created by Casian Colto on 31/05/2024.
//

import Foundation
import SwiftUI
import MapKit

// adding Identifiable will eliminate the need to add id parmater when initilizing a dynamic list
struct Landmark: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var park: String
    var state: String
    var description: String
    var isFavorite: Bool
    
    private var imageName: String
    var image: Image {
        Image(imageName)
    }
    

    private var coordinates: Coordinates
    var locationCoordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(
            latitude: coordinates.latitude,
            longitude: coordinates.longitude)
    }
    
        
    struct Coordinates: Hashable, Codable {
        var latitude: Double
        var longitude: Double
    }
}
