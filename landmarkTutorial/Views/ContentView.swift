//
//  ContentView.swift
//  landmarkTutorial
//
//  Created by Casian Colto on 30/05/2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        LandmarkList()
    }
}

#Preview {
    ContentView()
        .environment(ModelData())
}
