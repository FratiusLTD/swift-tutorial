//
//  CircleImage.swift
//  landmarkTutorial
//
//  Created by Casian Colto on 30/05/2024.
//

import SwiftUI

struct CircleImage: View {
    var image: Image
    
    var body: some View {
        image
            .clipShape(Circle())
            .overlay{
                Circle().stroke(.green, lineWidth: 5)
            }
            .shadow(radius: 6)
    }
}

#Preview {
    CircleImage(image: Image("turtlerock"))
}
