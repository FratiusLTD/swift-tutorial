//
//  LandmarkList.swift
//  landmarkTutorial
//
//  Created by Casian Colto on 31/05/2024.
//

import SwiftUI

struct LandmarkList: View {
    @Environment(ModelData.self) var modelData
    @State private var showFavoritesOnly = false
    
    var filteredLandmarks: [Landmark] {
        modelData.landmarks.filter { landmark in
            (!showFavoritesOnly || landmark.isFavorite)
        }
    }
    
    var body: some View {
//        Dynamiclly load by id all the landmarks in a LandmarkRow struct
        
// adding Identifiable to Landmark struct we can eliminate the id parmanter
        
        NavigationSplitView {
            List{
                
                Toggle(isOn: $showFavoritesOnly) {
                    Text("Favorites only")
                }
                
                ForEach(filteredLandmarks) { landmark in
                    NavigationLink {
                        LandmarkDetail(landmark: landmark)
                    } label: {
                        LandmarkRow(landmark: landmark)
                    }
                }
            }
            .animation(.default, value: filteredLandmarks)
            .navigationTitle("Landmarks")
            .padding()
            
        } detail: {
            Text("Select a Landmark")
                .padding()
        }
        .padding()
    }
}

#Preview {
    LandmarkList()
        .environment(ModelData())
}
