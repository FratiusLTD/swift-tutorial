//
//  landmarkTutorialApp.swift
//  landmarkTutorial
//
//  Created by Casian Colto on 30/05/2024.
//

import SwiftUI

@main
struct landmarkTutorialApp: App {
    
    @State private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(modelData)
        }
    }
}
